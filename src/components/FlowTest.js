// @flow

function foo(x: ?number): string {
  if (x) {
    return "Lolkek";
  }
  return 'Default msg';
}
