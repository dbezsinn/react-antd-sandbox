import React from "react";
import { version } from "antd";
import "antd/dist/antd.css";
import "./index.css";

import { TestForm } from "./components/TestForm";


export const App = props => {
  const handleFormChange = changedFields => {
    console.log("Form Change");
    console.log(changedFields);
  };
  const predefinedFields = {
    "test-inputField": {
      value: "HallJordan"
    }
  };

  return (
    <div className="App">
      <p>Current antd version: {version}</p>
      <p>Please fork this codesandbox to reproduce your issue.</p>
      <div>
        <TestForm {...predefinedFields} onChange={handleFormChange}/>
      </div>
    </div>
  );
};

export default App;
