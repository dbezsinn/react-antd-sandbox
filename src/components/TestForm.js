import React from "react";
import {Button, Form, Input} from "antd";
import "antd/dist/antd.css";

const CustomizedForm = Form.create({
  onFieldsChange(props, changedFields) {
    props.onChange(changedFields);
  },
  mapPropsToFields(props) {
    const fieldKeys = Object.keys(props).filter(key => key.match(/Field$/));
    const fields = {};

    console.log(fieldKeys);

    fieldKeys.forEach(key => {
      fields[key.slice(0, key.length - 5)] = Form.createFormField({
        ...props[key],
        value: props[key].value
      });
    });

    return fields;
  },
  onValuesChange(_, values) {
    console.log(values);
  }
})(props => {
  const {getFieldDecorator} = props.form;

  return (
    <Form layout="inline">
      <Form.Item>
        {getFieldDecorator("test-input", {
          getValueFromEvent: e => e.target.value.toUpperCase(),
          validateFirst: true,
          rules: [
            {required: true, message: "This field is required"},
            {
              whitespace: true,
              message: "Should contain something except whitespaces"
            },
            {
              pattern: /^[a-zA-Z]{5,20}$/,
              message: "Field must contain from 5 to 20 letters"
            }
          ]
        })(<Input/>)}
      </Form.Item>
      <Button type="primary">Here is a button</Button>
    </Form>
  );
});

export const TestForm = props => {
  const handleFormChange = changedFields => {
    console.log("Form Change");
    console.log(changedFields);
  };
  const predefinedFields = {
    "test-inputField": {
      value: "HallJordan"
    }
  };

  return (
      <CustomizedForm {...predefinedFields} onChange={handleFormChange}/>
  );
};

export default TestForm;
